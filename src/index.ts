import {sample} from 'lodash';
import ILogModel from './models/log';
import {
    accountNos,
    Action,
    actions,
    AuthenActionSubType,
    citizens,
    cloudActionSubTypes,
    mobileNos,
    OnboardActionSubType
} from './constants';
import {submitLog} from "./api";

// How many entries to add to the database
const entryCount: number = parseInt(process.env['ENTRY_COUNT']) || 5;
// Successful entries
let success = 0;

/**
 * Create a log object
 */
function createLog() {
    const startTime = new Date();

    const action = sample(actions);
    let subType: string = OnboardActionSubType.onboarding;
    switch (action) {
        case Action.authen:
            subType = AuthenActionSubType.login;
            break;
        case Action.cloud:
            subType = sample(cloudActionSubTypes);
            break;
    }

    const log: ILogModel = {
        actionType: action,
        firstName: 'Testy',
        lastName: 'McTester',
        displayName: 'Tester',
        citizenId: sample(citizens),
        fromAccountNo: sample(accountNos),
        method: 'get',
        mobileNo: sample(mobileNos),
        note: 'Test data, to be deleted',
        requestDatetime: startTime,
        responseDatetime: undefined,
        status: 'success',
        userId: 'dummy'
    };

    log.subType = subType;
    log.responseDatetime = new Date();

    return log;
}

(async () => {

    for (let i = 0; i < entryCount; i++) {
        const log = createLog();

        try {
            await submitLog(log);
            success++;
        } catch (e) {
            console.log(e);
        }
    }

    console.log(`Successfully added ${success} entries out of ${entryCount} required.`)
    console.log(`Success rate ${(success/entryCount) * 100}%`)
})();
