import fetch from 'node-fetch';
import ILogModel from '../models/log';

const url = process.env['API_URL'];

export const submitLog = (log: ILogModel)  => {
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(log),
    });
}
