export enum Action {
    authen = 'Authen',
    cloud = 'Cloud',
    onboard = 'Onboarding',
}

export enum AuthenActionSubType {
    login = 'login',
}

export enum CloudActionSubType {
    accept = 'accept',
    create = 'create',
    edit = 'edit',
    leave = 'leave',
    reject = 'reject',
    removeMember = 'remove member',
}

export enum OnboardActionSubType {
    onboarding = 'onboarding',
}

export const mobileNos = [
    '0811111111', '0822222222', '0833333333', '0812345678', '0898765432', '0800012234'
];

export const accountNos = [
    '048-1-48869-9', '048-1-48900-8', '048-1-48887-7', '048-1-48899-0'
]

export const citizens = [
    '8348754069451', '2121006867533'
]

export const actions = [
    Action.authen, Action.cloud, Action.onboard
];

export const cloudActionSubTypes = [
    CloudActionSubType.accept, CloudActionSubType.create, CloudActionSubType.edit,
    CloudActionSubType.leave, CloudActionSubType.reject, CloudActionSubType.removeMember
]
