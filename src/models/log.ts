export interface ILogModel {
    data?: string; // seems to be unneeded
    firstName?: string;
    lastName?: string;
    displayName?: string;
    citizenId?: string;
    mobileNo: string;
    fromAccountNo: string;
    toAccountNo?: string;
    actionType: string;
    subType?: string;
    userId: string; // hexadecimal
    status: string // 'success', 'error'
    method: string // http method
    requestDatetime: Date;
    responseDatetime: Date;
    ref1?: string;
    ref2?: string;
    note?: string;
}

export default ILogModel;
