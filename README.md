# Data Populator

This program sends mocked-up logs to NEO Back Office API.

## Building

```shell script
npm run build
```

## Running

Set `API_URL` (full URL to the Log endpoint) and `ENTRY_COUNT` (the number of entries to add)
environment variables before starting the program.

To start, run

```shell script
node dist/index.js
```

or

```shell script
npm start
```
